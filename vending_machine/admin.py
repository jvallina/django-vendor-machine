import random

from django.contrib import admin

from vending_machine.models import UserMoney, Product, Stock, SpaceChoices


class UserMoneyAdmin(admin.ModelAdmin):
    actions = []
    list_display = ['user', 'coin_value', 'quantity', 'coin_decimal_value']
    list_per_page = 50

    search_fields = ['user__username']

    list_filter = ['coin_value', ]

    readonly_fields = 'coin_decimal_value',


admin.site.register(UserMoney, UserMoneyAdmin)


class ProductAdmin(admin.ModelAdmin):
    actions = []
    list_display = ['name', 'price', ]
    list_per_page = 50

    search_fields = ['name']

    list_filter = ['price', ]


admin.site.register(Product, ProductAdmin)


def refill(modeladmin, request, queryset):
    products_id_list = list(Product.objects.values_list('id', flat=True))
    for choice in SpaceChoices:
        stock = Stock.objects.filter(space=choice).first()
        if stock:
            stock.quantity = 10
            stock.save()
        else:
            product_random_id = random.choice(products_id_list)
            stock = Stock.objects.create(space=choice, quantity=10, product_id=product_random_id)


class StockAdmin(admin.ModelAdmin):
    actions = [refill, ]
    list_display = ['space', 'product', 'quantity']
    list_per_page = 50

    search_fields = ['product__name']


admin.site.register(Stock, StockAdmin)
